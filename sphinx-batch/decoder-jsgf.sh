#!/bin/sh
pocketsphinx_batch \
    -samprate 8000 \
    -hmm model/model \
    -jsgf model/grammar.jsgf \
    -dict model/happy_presidents.dic \
    -cepdir audios \
    -cepext .wav \
    -ctl test.listoffiles  \
    -hyp result.jsgf.hyp\
    -hypseg result.txt \
    -nbest 4\
    -nbestdir nbesthyp\
    -logfn sphinx.log \
    -adcin yes 
