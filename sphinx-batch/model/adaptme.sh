#!/bin/sh
#set +x
if [ $1 = 'clean' ]; then
  echo CleanUp
  rm ./*.mfc
  rm ./*_counts
  exit 
fi

#модель тренировки
AMODEL=model
#каталог с корпусом тренировки
VOICE_DIR=voices
#Базовый файл корпуса тренировки
ARCTIC=arcticX
#Словарь языка
DICTONARY=happy_presidents.dic 
#sphinx train
SPHINX_TRAIN=/usr/local/libexec/sphinxtrain

#get mdef.txt from model - optional, if need
echo pocketsphinx_mdef_convert
pocketsphinx_mdef_convert -text $AMODEL/mdef $AMODEL/mdef.txt 2> adaptme.log
if ! [ $? -eq 0 ];  then 
  echo pocketsphinx_mdef_convert: error happend, see apadtme.log
  exit
fi

#wav->mfc prepare acoustic data
echo sphinx_fe
sphinx_fe -argfile $AMODEL/feat.params  -samprate 8000 -c $VOICE_DIR/$ARCTIC.listoffiles -di $VOICE_DIR -do . -ei wav -eo mfc -mswav yes 2> adaptme.log
if ! [ $? -eq 0 ];  then 
  echo sphinx_fe: error happend, see apadtme.log
  exit
fi

#get statistics 
echo bw
$SPHINX_TRAIN/bw -hmmdir $AMODEL -moddeffn $AMODEL/mdef.txt  -ts2cbfn .cont. -feat 1s_c_d_dd -cmn current -agc none -lda  $AMODEL/feature_transform -dictfn $DICTONARY -ctlfn $VOICE_DIR/$ARCTIC.listoffiles -lsnfn $VOICE_DIR/$ARCTIC.transcription  -accumdir . 2>> adaptme.log 1> bw.log
if ! [ $? -eq 0 ] ; then 
  echo bw: error happend, see apadtme.log
#  goto L_exit
  exit
fi

#make map adaptation
echo map_adapt
$SPHINX_TRAIN/map_adapt -meanfn $AMODEL/means -varfn $AMODEL/variances -mixwfn $AMODEL/mixture_weights -tmatfn $AMODEL/transition_matrices -accumdir . -mapmeanfn $AMODEL/means -mapvarfn $AMODEL/variances -mapmixwfn $AMODEL/mixture_weights -maptmatfn $AMODEL/transition_matrices 2>> adaptme.log 1> map_adapt.log
if ! [ $? -eq 0 ];  then 
  echo map_adapt: error happend, see apadtme.log
  exit
#  goto L_exit
fi

rm ./*.mfc
rm ./*_counts
