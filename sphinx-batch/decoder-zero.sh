#!/bin/sh
pocketsphinx_batch \
    -samprate 8000 \
    -hmm model/model \
    -lm  model/msu_ru_zero.lm.dmp \
    -dict model/happy_presidents.dic \
    -cepdir audios \
    -cepext .wav \
    -ctl test.listoffiles  \
    -hyp result.zero.hyp\
    -hypseg result.zero.txt \
    -nbest 4\
    -nbestdir nbesthyp\
    -logfn sphinx.log \
    -adcin yes 
