#!/bin/sh
pocketsphinx_batch \
    -samprate 8000 \
    -hmm model/model \
    -allphone model/msu_ru_zero.lm.dmp \
    -backtrace yes \
    -beam 1e-20 -pbeam 1e-20 -lw 2.0 \
    -cepdir audios \
    -cepext .wav \
    -ctl test.listoffiles  \
    -hyp result.allphone.hyp\
    -hypseg result.allphone.txt \
    -nbest 4\
    -nbestdir nbesthyp\
    -logfn sphinx.log \
    -adcin yes 
