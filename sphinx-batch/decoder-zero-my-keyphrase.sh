#!/bin/sh

rm sphinx.log
pocketsphinx_batch \
    -samprate 8000 \
    -hmm model/model \
    -dict model/happy_presidents.dic\
    -cepdir audios \
    -cepext .wav \
    -ctl test.listoffiles  \
    -hyp result.zero.kws.hyp\
    -hypseg result.zero.kws.txt \
    -nbest 4\
    -nbestdir nbesthyp\
    -logfn sphinx.log \
    -keyphrase рейган \
    -kws_threshold 1e-10 \
    -adcin yes 

    #    -lm  model/msu_ru_zero.lm.dmp \
