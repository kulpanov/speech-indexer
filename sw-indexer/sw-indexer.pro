#-------------------------------------------------
#
# Project created by QtCreator 2015-03-15T18:17:19
#
#-------------------------------------------------

CONFIG += debug_and_release

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sw-indexer
TEMPLATE = app

INCLUDEPATH += /usr/local/include/pocketsphinx /usr/local/include/sphinxbase

SOURCES += src/main.cpp\
				src/utils/SSpinValue.cpp\
				src/utils/CException.cpp\
				src/analyzeIt.cpp\
        src/mainwindow.cpp

SOURCES += tests/selftest.cpp\
        tests/SpinValue_test.cpp\
        tests/CException_test.cpp

HEADERS  += src/mainwindow.h

FORMS    += src/ui/mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11

QMAKE_LIBDIR += /usr/local/lib
LIBS += -lpocketsphinx -lsphinxbase
