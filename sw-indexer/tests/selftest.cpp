/*
 * selftest.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */
#include <string.h>
#include <map>
#include <iostream>
#include <cassert>
#include "selftest.h"


// in cpp file
ITestRunner::tTestList ITestRunner::testsMap;


int main_test(int argc, char *argv[]){
	assert(argc > 1);
	assert(strcmp(argv[1],"selftest")==0);
	try{
			if(argc==2)//all
				for(auto t: ITestRunner::testsMap)	t.second->run();
			else{
				auto arg_vector = std::vector<std::string>(argv+2, argv+argc);
				for(auto t: arg_vector)
					ITestRunner::testsMap[t];
			}
	}catch(std::exception& e){
		std::cerr << "FAIL:" << e.what();
	}
	exit(0);
}
