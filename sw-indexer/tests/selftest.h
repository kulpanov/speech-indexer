/*
 * selftest.h
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */

#pragma once
#include <map>
#include <vector>

//in h file
class ITestRunner{
public:
	typedef std::map<std::string, ITestRunner*> tTestList;
	typedef std::pair<std::string, ITestRunner*> tTestPair;
public:
	virtual int run() = 0;
	virtual ~ITestRunner(){	}
public:
	static tTestList testsMap;
};

