/*
 * CException_test.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */

#include <string.h>
#include <cassert>
#include <iostream>
#include "../src/utils/debug.h"

#include "selftest.h"

//
class TestException: public ITestRunner{
public:
	virtual int run(){;
		std::cout << "TestException..." << std::endl;

		try{
			throw E("hello") << E_FUNC << E_LF;
		}catch (std::exception& e) {
			assert(strncmp(e.what(), "hello", 5) == 0);
			std::cout << e.what() << std::endl;
		}
		std::cout << "pass" << std::endl;
		return true;
	}
	TestException(){
		ITestRunner::testsMap.insert({"TestException",  this});
	}
};
TestException testException;
