/*
 * SpinValue.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */
#include <cassert>

#include "selftest.h"

#include "src/utils/SSpinValue.h"

//
class TestSpinValue: public ITestRunner{
public:
	virtual int run(){
			std::cout << "TestSpinValue...";
			assert(SpinValue::fromStr( "/1e-100/") == 0);
			assert(SpinValue::fromStr( "/1e100/") == 100);
			assert(SpinValue::fromStr( "/1e0/") == 50);
			assert(SpinValue::fromStr( "/1e+0/") == 50);
			assert(SpinValue::fromStr( "/1e-2/") == 49);
			assert(SpinValue::fromStr( "/1e+2/") == 51);
			//waste values
			assert(SpinValue::fromStr( "") == 50);
			assert(SpinValue::fromStr( "skfjnad;fd;jn") == 50);
			//...
			assert(SpinValue::fromCell(0) == "/1e-100/");
			assert(SpinValue::fromCell(100) == "/1e100/");
			assert(SpinValue::fromCell(50) == "/1e0/");
			assert(SpinValue::fromCell(49) == "/1e-2/");
			//waste values
			assert(SpinValue::fromCell(200) == "/1e100/");
			assert(SpinValue::fromCell(-3100) == "/1e-100/");
			std::cout << "pass" << std::endl;
			return true;
	}
	TestSpinValue(){
		ITestRunner::testsMap.insert(tTestPair("TestSpinValue",  this));
	}
};
TestSpinValue testSpinRunner;


