/*
 * CException.h
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */

#ifndef SRC_UTILS_DEBUG_H_
#define SRC_UTILS_DEBUG_H_


#include <exception>
#include <sstream>
#include <string>

///Вспомогательный класс обработки исключений
class CException : public std::exception
{
protected:
	std::stringstream ss;
public:
	CException& operator <<(const std::string& msg){
		ss << msg;
		return *this;
	}

	CException& operator <<(const char* msg){
		ss << msg;
		return *this;
	}

	CException& operator <<(const int imsg){
		ss << imsg;
		return *this;
	}

	virtual const char* what() const _GLIBCXX_USE_NOEXCEPT{
//		const char* s = ss.str().c_str();
		return ss.str().c_str();
	}

public:
	//int _code = 0, const char* _msg = NULL, int line = 0, const char* file = NULL
	inline CException (const char* _msg = NULL) _GLIBCXX_USE_NOEXCEPT{
		if(_msg) ss << _msg;
	}
	inline CException (const CException& _exc) _GLIBCXX_USE_NOEXCEPT
	{	ss << _exc.ss.str();
	}
//public:
//	static CException& addFileLine(CException& exc, int _line, const char* _file){
//		exc << "\n\tat " << _file << ":" << _line << endl;
//		return exc;
//	}
};

///Исключение: файл не найден
class CException_fileNotFound : public CException{
public:
	inline CException_fileNotFound(const char* _fname):CException("file not found:")
	{	ss << _fname;}
	inline CException_fileNotFound(const std::string& _fname):CException("file not found:")
	{	ss << _fname;}
//	CException_fileNotFound(CException_fileNotFound& exc)
//	{	ss << exc.ss;}
};
#define EXC_FNF(fname) CException_fileNotFound(fname)

///вспомогательне макросы
#define E(_msg) (CException(_msg))
#define E_LF "\n\tat " << __LINE__ << ":" << __FILE__
#define E_FUNC ":" << __func__ << ":"



#endif /* SRC_UTILS_DEBUG_H_ */
