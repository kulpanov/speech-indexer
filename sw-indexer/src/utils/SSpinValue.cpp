/*
 * SSpinValue.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */

#include "SSpinValue.h"

inline void SpinValue_checkRange(int& _cellValue){
	if(_cellValue < 0) 	_cellValue = 0;
	if(_cellValue > 100)_cellValue = 100;
}
//
QString SpinValue::fromCell(int _cellValue){//combine string: 0..100 -> [1-e100...1e100]
	SpinValue_checkRange(_cellValue);
	_cellValue = _cellValue*2 - 100 ;
//		std::stringstream ss("/1");
//		if(value<0) ss << '-';
//		ss << 'e' << value;
//		return ss.str();
	return QString("/1e%1/").arg(_cellValue);
}

//
int SpinValue::fromStr(const std::string& _val){
	int value = 50;//zero!
	try{// extract power from [1-e100...1e100] -> 0..100
		int epos = _val.find('e');
//		int sign = _val[epos-1]=='-'?-1:+1;
		value = (atoi(_val.substr(epos+1).c_str()) + 100)/2;
	}catch(std::exception& e){
		std::cerr<<"CSpinValue:"<<value<<":exception="<<e.what();
	}
	return value;
}
