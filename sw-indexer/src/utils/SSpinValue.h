/*
 * SSpinValue.h
 *
 *  Created on: Jun 18, 2015
 *      Author: kulpanov
 */

#pragma once
#include <iostream>
#include <QtCore/QString>

///helper class for convers operations with power of e
struct SpinValue{
	//combine string: 0..100 -> [1-e100...1e100]
	static QString fromCell(int _cellValue);
	//
	static int fromStr(const std::string& _val);

	friend class TestSpinValue;
};
