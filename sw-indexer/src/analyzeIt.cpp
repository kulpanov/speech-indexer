#include <cassert>
#include <stdexcept>
#include <cstdio>
#include <future>

#include <QtCore/QFile>
#include <QtCore/qtextstream.h>
#include <src/utils/debug.h>
#include <QtGui/QSpinBox>
#include <QtCore/QTimer>

using namespace std;
#include <pocketsphinx_export.h>
#include <pocketsphinx.h>
#include <ps_search.h>
#include <sphinxbase/cmd_ln.h>

#include "src/utils/SSpinValue.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

static const char* MODEL_KEYPHRASES_TXT = "model/keyphrases.txt";
static const char* searchname= "kws_search";

static cmd_ln_t *config = NULL;
static ps_decoder_t* ps = NULL;

void
MainWindow::initSphinx ()
{ assert(config == NULL);

	try {

	config = cmd_ln_init (NULL, ps_args (), TRUE
												, "-hmm", MODEL "/model"
												, "-dict", MODEL "/happy_presidents.dic"
//												, "-kws", MODEL "/keyphrases.txt"
												, "-samprate", "8000", NULL);

	if (config == NULL)
		throw E("sphinx config init") << E_FUNC;

	ps = ps_init (config);
	if (ps == NULL)
		throw E("sphinx ps_init") << E_FUNC;;

	} catch (exception& e) {
			doneSphinx();
		throw(e);
	}

}
void MainWindow::doneSphinx(){
	if(config) {
		cmd_ln_free_r(config);
		config = NULL;
	}
	if(ps) {
		ps_free(ps);
		ps = NULL;
	}
}

///Сформировать вывод времени появления слова
static void
print_word_times (ps_decoder_t *ps, list<pair<QString, QString> >& sl)
{
	const int frame_rate = 10;//1/100sec
	const int offs = 4 * frame_rate;
	//  FILE* fres = fopen("result.txt", "a");
	ps_seg_t *iter = ps_seg_iter (ps, NULL);
	while (iter != NULL)
	{
		const char * segment = ps_seg_word (iter);
		if( segment && strlen(segment)>0){
			int32 sf, ef;//, pprob, ascore;
			ps_seg_frames (iter, &sf, &ef);
//			pprob = ps_seg_prob (iter, &ascore, NULL, NULL);
//			double conf = logmath_exp (ps_get_logmath (ps), pprob);
			//      fprintf (fres, "%s\t%.3f\n", ps_seg_word (iter), ((float) sf / frame_rate));
			QString srec = QString().fromUtf8(ps_seg_word (iter));
			QString timerec = QString().number(double(sf*frame_rate+offs)/1000);
//
//					QString("%0 = sw:%1,ew:%2,probe:%3,ascore:%4\n")
//												.arg(QString().fromUtf8(ps_seg_word (iter)))
//												.arg(sf*frame_rate+offs)
//												.arg(ef*frame_rate+offs)
//												.arg(conf)
//												.arg(ascore);
			sl.push_front({srec, timerec});
		}
		iter = ps_seg_next (iter);
	}
}


void
MainWindow::analyzeIt ()
{
	///Guard структура синхронизирующая статус программы
	struct SFuncGuard{
		Ui::MainWindow* ui;
		SFuncGuard(Ui::MainWindow* _ui): ui(_ui){
			ui->statusBar->showMessage(tr("Analyzing ..."));
			ui->bAnalyzeIt->setEnabled(false);
			ui->bShowStat->setEnabled(false);
			QEventLoop loop;QTimer::singleShot(100, &loop, SLOT(quit()));loop.exec();
		}
		~SFuncGuard(){
			ui->statusBar->showMessage(tr("Ready"));
			ui->bAnalyzeIt->setEnabled(true);
			ui->bShowStat->setEnabled(true);
		}
	};

	SFuncGuard guard(ui);
	try{
			//инициализирует сфинск, если еще небыло - lazyCreation
		if(NULL == ps) initSphinx();
		//заполняем файл индекса-словаря
		{	QFile indexFile (MODEL_KEYPHRASES_TXT);
			if (! indexFile.open(QFile::WriteOnly | QFile::Truncate))
				throw EXC_FNF(indexFile.fileName().toUtf8().begin()) << E_LF;

			QTextStream out(&indexFile);

			for(int i=0; i<ui->index->rowCount(); i++){
				QString indxWord = ui->index->item(i, 0)->text();
				out << indxWord << " "
						<< SpinValue::fromCell( qobject_cast<QSpinBox*>(ui->index->cellWidget(i, 1))->value() ) << endl;
			}
		}

		//устанавливаем его сфинску как рабочий
		if (ps_set_kws (ps, searchname, MODEL_KEYPHRASES_TXT))
				throw E("analyzeIt:ps_set_kws fault");

		//Запускаем процесс декодирования и поиска
		ps_set_search(ps, searchname);

		list<pair<QString, QString> > sl;
		auto indexes = ui->lvFiles->selectionModel()->selectedIndexes();
		for(auto findex: indexes){
			auto wav_name =  fs_model->rootPath() + '/' +findex.data().toString();
			FILE* fh = fopen (wav_name.toUtf8().begin(), "rb");
			if (fh == NULL)	throw EXC_FNF(wav_name.toUtf8().begin()) << E_LF;
			ps_decode_raw(ps, fh, -1);
			fclose(fh);
			print_word_times(ps, sl);
		}
		ps_unset_search(ps, searchname);
		//отображаем результаты
		viewResult(sl);
	}catch(std::exception& e){
		std::cerr << e.what() << endl;
	}


}
