#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>

#include <tests/selftest.h>

extern int main_test(int argc, char *argv[]);

///Основная запускаемая утилита
int main(int argc, char *argv[])
{
	//режим самотестирования
		if(argc>1 && strcmp(argv[1], "selftest") == 0){
				main_test(argc, argv);
		}
	//запуск основного окна приложения
    QApplication a(argc, argv);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    MainWindow w;
    w.show();
    return a.exec();
}
