#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cassert>

#include <QtCore/QDebug>
#include <QtCore/QProcess>
#include <QtGui/QStandardItemModel>
#include <QtGui/QTableWidget>
#include <QtGui/QSpinBox>
#include <QtCore/QTimer>
#include "src/utils/SSpinValue.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"

// /home/kulpanov/_prj/amt/sr/ws/sphinx-batch/

static QSpinBox*
createThressholdWidget (QWidget* owner, std::string threshold)
{
	//extract power from  /1-e1/
	QSpinBox* sp = new QSpinBox (owner);
	sp->setMaximum (100); sp->setMinimum (0);
	sp->setValue (SpinValue::fromStr(threshold));
	return sp;
}

MainWindow::MainWindow (QWidget *parent) :
    QMainWindow (parent), ui (new Ui::MainWindow)
{
  ui->setupUi (this);
  setWindowTitle(QString::fromUtf8("WordIndexer"));
  //setup fs catalog ------------------
  fs_model = new QFileSystemModel;
  ui->lvFiles->setModel (fs_model);
  ui->lvFiles->setRootIndex (fs_model->setRootPath ("audios"));
  ui->lvFiles->setSelectionBehavior(QAbstractItemView::SelectItems);
  ui->lDirName->setText (fs_model->rootPath ());
  ui->lvFiles->setCurrentIndex(fs_model->index(0,0));

  modelResult = new QStandardItemModel(0, 2, this); //2 Rows and 3 Columns
  modelResult->setHorizontalHeaderItem(0, new QStandardItem(QString::fromUtf8("Метка")));
  modelResult->setHorizontalHeaderItem(1, new QStandardItem(QString::fromUtf8("Время, сек")));
  ui->vResult->setModel(modelResult);

  //setup index tableQString() -------------------
	ui->index->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Слово")));
	ui->index->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Порог")));

	std::string keyphrase, threshold;
	std::ifstream indexFile( MODEL "/keyphrases.txt");
	while(indexFile >> keyphrase >> threshold){ //loopfor keys
		std::string::size_type pos = 0;
		auto rowCount = ui->index->rowCount();
		ui->index->insertRow(rowCount);
		ui->index->setItem(rowCount, 0, new QTableWidgetItem(QString().fromUtf8(keyphrase.data())));
		ui->index->setCellWidget ( rowCount, 1, createThressholdWidget (this, threshold) );
		//ui->index->setItem(0, 1, new QTableWidgetItem(QString().fromStdString(threshold)));
	}
	ui->index->horizontalHeader()->setResizeMode( 0, QHeaderView::Stretch );
	ui->index->horizontalHeader()->setResizeMode( 1, QHeaderView::ResizeToContents );

	//setup buttons
  connect (ui->bSwitchCatalog, SIGNAL(clicked()), this, SLOT(playIt()));
  connect (ui->bAnalyzeIt, SIGNAL(clicked()), this, SLOT(analyzeIt()));

  ui->statusBar->showMessage(tr("Ready"));
}

MainWindow::~MainWindow ()
{
	doneSphinx();
  delete ui;
//  delete model;
}

void
MainWindow::viewResult (const std::list<std::pair<QString, QString> > & sl)
{
//	modelResult
	modelResult->clear();
  modelResult->setHorizontalHeaderItem(0, new QStandardItem(QString::fromUtf8("Метка")));
  modelResult->setHorizontalHeaderItem(1, new QStandardItem(QString::fromUtf8("Время, сек")));
	for(auto a:sl){
		auto rowCount = modelResult->rowCount();
		modelResult->insertRow(rowCount);
		modelResult->setItem(rowCount, 0, new QStandardItem(a.first));
		modelResult->setItem(rowCount, 1, new QStandardItem(a.second));

		//ui->index->setItem(0, 1, new QTableWidgetItem(QString().fromStdString(threshold)));
	}

}

class CPlayProcess:public QProcess{
	Ui::MainWindow *ui;
public:
	CPlayProcess(Ui::MainWindow *_ui):ui(_ui){
		ui->statusBar->showMessage(tr("Playing ..."));
		ui->bSwitchCatalog->setEnabled(false);
	}
	~CPlayProcess(){
		ui->statusBar->showMessage(tr("Ready"));
		ui->bSwitchCatalog->setEnabled(true);
	}
};

void
MainWindow::playIt(){
	//
	QProcess* procPlay = new CPlayProcess(ui);

	QString startCmd("play ");
	auto indexes = ui->lvFiles->selectionModel()->selectedIndexes();
	for(auto findex: indexes){
		startCmd += fs_model->rootPath() + '/' +findex.data().toString() + " ";
	}
	connect(procPlay, SIGNAL(finished(int)), procPlay, SLOT(deleteLater()));
	procPlay->start(startCmd);
}


