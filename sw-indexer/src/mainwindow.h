#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QtGui/QStandardItemModel>
#include <list>

#define MODEL "model"

namespace Ui {
class MainWindow;
}

/** Основное окно приложения
 *
 */
class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private:
	///ui приложеня автогенерирован из designer
	Ui::MainWindow *ui;
	///модель ФС для доступа к файлам фонограм
	QFileSystemModel* fs_model;
	///модель результатов работы, 2d-таблица
	QStandardItemModel *modelResult;

	///иниц сфинкс подсистему, вызывается при первом запуске "Анализироать"
	static void initSphinx();
	///завершаем работу сфинкс подсистемы
	static void doneSphinx();
	///отображаем результат во вью модели modelResult
	void viewResult(const std::list<std::pair<QString, QString> > & sl);

private slots:
	///Обработчик "Аналаизировать"
	void analyzeIt();
	///"Проиграть"
	void playIt();
};

#endif // MAINWINDOW_H
