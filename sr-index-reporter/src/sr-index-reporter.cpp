//============================================================================
// Name        : sr-index-reporter.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <list>

using namespace std;
#define MODEL "/home/kulpanov/_prj/amt/sr/ws/sphinx-batch/model"

const string extractName(string _transcript){
	auto pos = _transcript.find("(");
	return _transcript.substr(pos, _transcript.find(')') - pos);
}

std::list<std::string> keyphrases;
void loadPhrases()
{	string keyphrase, threshold;
	ifstream indexFile( MODEL "/keyphrases-vars.txt");
	while(indexFile >> keyphrase >> threshold){ //loopfor keys
			keyphrases.push_back(" " + keyphrase);
	}
}

int main() {
	loadPhrases();

	string transcription;
	ifstream transcriptFile( MODEL "/arcticX.transcription");
	list<list<pair<const string, int> > > lines;

	while(getline(transcriptFile, transcription))
	try{//loopfor phrases
		list<pair<const string, int> > items;
//		cout << transcription << endl;
	  items.push_back( {extractName(transcription) + string("="), -1});
		string keyphrase, threshold;
		for(auto keyphrase: keyphrases){ //loopfor keys
//			cout << keyphrase << ":" << threshold << endl;
				string::size_type pos = 0;
				while(string::npos != (pos = transcription.find(keyphrase, pos))){
					items.push_back({keyphrase, pos});

					pos += keyphrase.length();
				}
				//sort line
				items.sort([](const pair<const string, int>& f, const pair<const string, int>& s){
					return f.second < s.second;
				});
		}
		lines.push_back(items);
	}catch(std::exception& e){
		cerr << e.what();
	}
//ostream& out = [=]() -> ostream& { if (argc>1) { static fstream fs(argv[1]); return fs; } return cout;}();
	{//ouput index file

		ofstream indexFile(MODEL "/index-report.txt");
		for(auto it=lines.begin(); it != lines.end(); ++it){
			stringstream ss;
			for(auto it2=it->begin(); it2 != it->end(); ++it2){
				ss << (*it2).first;
				if((*it2).second>0) ss << ":" <<(*it2).second;
				ss << ",";
			}
			indexFile << ss.rdbuf() << endl;
		}
	}

	return 0;
}
